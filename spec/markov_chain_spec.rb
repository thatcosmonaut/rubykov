require 'spec_helper'

describe Rubykov::MarkovModel do
  let(:trained_markov_chain) do
    Rubykov::MarkovModel.new(2, ['sunny','rainy','sunny','sunny','sunny','windy','sunny','rainy','windy','rainy'])
  end

  describe 'initialize' do
    context 'given an array of training data' do
      it 'generates the correct markov model' do
        expect(trained_markov_chain.transitions).to include(
          ['sunny','rainy'] => ['sunny','windy'],
          ['rainy','sunny'] => ['sunny'],
          ['sunny','sunny'] => ['sunny', 'windy'],
          ['sunny','windy'] => ['sunny'],
          ['windy','sunny'] => ['rainy'],
          ['rainy','windy'] => ['rainy']
        )
      end
    end
  end

  describe 'chain_with_seed' do
    context 'given a seed which is in the model' do
      it 'returns enumerator beginning with seed' do
        expect(trained_markov_chain.chain_with_seed(['rainy','sunny']).take(3)).to eql ['rainy', 'sunny', 'sunny']
      end
    end
  end

  describe 'train' do
    context 'given an already trained markov model' do
      context 'given more data' do
        it 'adds the new information to the markov model' do
          trained_markov_chain.train(['sunny','rainy','windy','rainy'])
          expect(trained_markov_chain.transitions).to include(
            ['sunny','rainy'] => ['sunny','windy','windy'],
            ['rainy','sunny'] => ['sunny'],
            ['sunny','sunny'] => ['sunny', 'windy'],
            ['sunny','windy'] => ['sunny'],
            ['windy','sunny'] => ['rainy'],
            ['rainy','windy'] => ['rainy','rainy']
          )
        end
      end
    end
  end
end
