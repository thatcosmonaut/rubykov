require 'spec_helper'

describe Rubykov::HiddenMarkovModel do
  let(:hidden_markov_model) do
    states = [:healthy, :fever]
    observations = [:normal, :cold, :dizzy]
    start_probability = { healthy: 0.6, fever: 0.4 }
    transition_probabilities = {healthy: { healthy: 0.7, fever: 0.3},
                                fever: { healthy: 0.4, fever: 0.6 }
    }
    observation_probabilities = {healthy: { normal: 0.5, cold: 0.4, dizzy: 0.1 },
                                 fever: { normal: 0.1, cold: 0.3, dizzy: 0.6 }
    }
    Rubykov::HiddenMarkovModel.new(states,
                                   observations,
                                   start_probability,
                                   transition_probabilities,
                                   observation_probabilities
                                  )
  end

  describe 'most_likely_state_sequence' do
    context 'given a sequence of observations' do
      it 'returns the most likely state sequence' do
        expect(hidden_markov_model.most_likely_state_sequence(['normal','cold','dizzy'])).to eql [:healthy,:healthy,:fever]
      end
    end
  end

  describe 'most_likely_state_sequence_probability' do
    context 'given a sequence of observations' do
      it 'returns the probability of the most likely state sequence' do
        expect(hidden_markov_model.most_likely_state_sequence_probability(['normal','cold','dizzy'])).to eql 0.01512
      end
    end
  end
end
