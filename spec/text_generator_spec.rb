require 'spec_helper'

describe Rubykov::TextGenerator do
  let(:trained_text_generator) do
    Rubykov::TextGenerator.new(2, 'The quick brown fox jumps over the brown fox who is slow jumps over the brown fox who is dead.')
  end

  describe 'initialize' do
    context 'given an order of 2 and a string of training data' do
      it 'generates the correct states' do
        expect(trained_text_generator.states).to match_array(
          [
            ["the", "quick"],
            ["quick", "brown"],
            ["brown", "fox"],
            ["fox", "jumps"],
            ["jumps", "over"],
            ["over", "the"],
            ["the", "brown"],
            ["fox", "who"],
            ["who", "is"],
            ["is", "slow"],
            ["slow", "jumps"],
            ["is", "dead"],
          ]
        )
      end

      it 'generates the correct transitions' do
        expect(trained_text_generator.transitions).to include(
          ['the','quick'] => ['brown'],
          ['quick','brown'] => ['fox'],
          ['brown','fox'] => ['jumps','who','who'],
          ['fox', 'jumps'] => ['over'],
          ['jumps', 'over'] => ['the', 'the'],
          ['over', 'the'] => ['brown', 'brown'],
          ['the', 'brown'] => ['fox', 'fox'],
          ['fox', 'who'] => ['is', 'is'],
          ['who', 'is'] => ['slow', 'dead'],
          ['is', 'slow'] => ['jumps'],
          ['slow', 'jumps'] => ['over'],
          ['is', 'dead'] => ['.']
        )
      end
    end
  end

  describe 'train' do
    context 'given an already trained text generator' do
      context 'given more data' do
        it 'adds the new information to the generator' do
          trained_text_generator.train('The brown fox jumps.')
          expect(trained_text_generator.transitions).to include(
            ['the','quick'] => ['brown'],
            ['quick','brown'] => ['fox'],
            ['brown','fox'] => ['jumps','who','who', 'jumps'],
            ['fox', 'jumps'] => ['over', '.'],
            ['jumps', 'over'] => ['the', 'the'],
            ['over', 'the'] => ['brown', 'brown'],
            ['the', 'brown'] => ['fox', 'fox', 'fox'],
            ['fox', 'who'] => ['is', 'is'],
            ['who', 'is'] => ['slow', 'dead'],
            ['is', 'slow'] => ['jumps'],
            ['slow', 'jumps'] => ['over'],
            ['is', 'dead'] => ['.'],
          )
        end
      end
    end
  end

  describe 'character_limited_output' do
    context 'markov chain has been generated' do
      context 'given a length' do
        it 'returns a sentence of equal or less character length' do
          expect(trained_text_generator.character_limited_output(140).length).to be <= 140
        end

        it 'does not return an empty string' do
          expect(trained_text_generator.character_limited_output(140).length).to be > 0
        end
      end
    end
  end

  describe 'word_output' do
    context 'markov chain has been generated' do
      context 'given a length' do
        it 'returns a sentence of equal or less word length' do
          expect(trained_text_generator.word_limited_output(8).split(' ').length).to be <= 8
        end

        it 'does not return an empty string' do
          expect(trained_text_generator.word_limited_output(8).split(' ').length).to be > 0
        end
      end
    end
  end

  describe 'sentence_output' do
    context 'markov chain has been generated' do
      context 'given a length' do
        it 'returns a string consisting of the given number of sentences.' do
          expect(trained_text_generator.sentence_limited_output(3).split('.').length).to eql 3
        end
      end
    end
  end
end
