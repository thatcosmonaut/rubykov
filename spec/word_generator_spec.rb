require 'spec_helper'

describe Rubykov::WordGenerator do
  let(:trained_word_generator) do
    Rubykov::WordGenerator.new(2, ['aerial', 'aerienne', 'area'])
  end

  describe 'train' do
    context 'given a list of words' do
      it 'adds the information to the model' do
        trained_word_generator.train(['aerie'])
        expect(trained_word_generator.transitions).to include(
          ['a','e'] => ['r','r','r'],
          ['e','r'] => ['i','i','i'],
          ['r','i'] => ['a','e','e'],
          ['i','e'] => ['n', '\\n']
        )
      end
    end
  end
end
