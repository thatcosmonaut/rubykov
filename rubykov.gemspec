Gem::Specification.new do |s|
  s.name        = 'rubykov'
  s.version     = '0.2.0'
  s.date        = Date.today.to_s
  s.summary     = "Sensible, easy Markov models in Ruby."
  s.description = "This gem defines Markov model-related classes."
  s.authors     = ["Evan Hemsley"]
  s.email       = 'evan.hemsley@gmail.com'
  s.files       = Dir.glob("{lib}/**/*") + %w(license.md README.md)
  s.homepage    =
    'https://gitlab.com/ehemsley/rubykov'
  s.license       = 'MIT'

  s.add_development_dependency 'rspec', '~> 3.1.0', '>= 3.1.0'
end
